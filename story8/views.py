from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import json, urllib
import requests
import urllib.request

# Create your views here.
def story8 (request):
    context = {
    }
    return render(request, 'story8.html', context)

def search (request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    response = requests.get(url)
    data = json.loads(response.content)
    return JsonResponse(data, safe=False)