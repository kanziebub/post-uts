from django.contrib import admin
from django.urls import path
from . import views

app_name = 'story8'

urlpatterns = [
    path('library/', views.story8, name='library'),
    path('data/', views.search, name='data'),
]
