from django.contrib import admin
from django.urls import path
from . import views

app_name = 'story7'

urlpatterns = [
    path('my-profile/', views.story7, name='profile')
]
