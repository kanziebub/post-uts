from django.contrib import admin
from django.urls import path, include
from . import views

app_name = 'story9'

urlpatterns = [
    path('', views.story9, name='welcome'),
    path('signup/', views.signup, name='signup'),
]