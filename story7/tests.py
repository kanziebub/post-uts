from django.test import TestCase, Client

from . import views, apps

# Create your tests here.
class Story7Tests (TestCase):
    # --------------- Testing ---------------
    def test_url_exists_in_story7 (self):
        response = self.client.get('/my-profile/')
        self.assertEquals(response.status_code, 200)
