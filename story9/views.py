from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm

# Create your views here.
def story9(request):
    return render(request, 'story9.html')

def signup(request):
    if (request.method == 'POST'):
        form = UserCreationForm(request.POST)
        
        if (form.is_valid):
            form.save()
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']

            user = authenticate(username=username, password=password)
            login(request, user)

            return redirect('/')
    else:
        form = UserCreationForm()
    context = {
        'form' : form,
    }
    return render(request, 'signup.html', context)